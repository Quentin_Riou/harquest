export type UserProps = {
    id: number;
    pseudo: string;
    score: number;
  };

export type ReponseProps = {
    id: number;
    responsePossible: string[];
    bonneReponse: string;
    explication: string;
    question: {
        id?: number;
    };
};

export type QuestionProps = {
    id: number;
    contexte: string;
    libelle: string;
    reponses: ReponseProps[];
  };
  
  /*export type BeerWithoutIdProps = Omit<BeerFullProps, 'id'>;
  
  export type BeerProps = {
    id?: number;
    name: string;
    style: string;
  };
  
  export type BreweryProps = {
    id?: number;
    name: string;
    location: string;
    beers: BeerProps[];
  };
  
  export type OrderProps = {
    id?: number;
    customerName?: string;
    date?: string;
    beers: BeerProps[];
  };*/