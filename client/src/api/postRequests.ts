import axios from 'axios';
import { baseUrl } from './constants';
import { UserProps } from './types';

// POST ENDPOINTS -- User
export async function postAUser({ pseudo }: UserProps) {
  return await axios
    .post(`${baseUrl}users`, {
      pseudo: `${pseudo}`
    })
    .then((res) => res.data);
}