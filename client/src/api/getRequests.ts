import axios from 'axios';
import { baseUrl } from './constants';

// GET ENDPOINTS -- Reponse
export async function getAllReponses() {
  return await axios.get(`${baseUrl}reponses`).then((res) => res.data);
}

// GET ENDPOINTS -- Question
export async function getAllQuestions() {
  return await axios.get(`${baseUrl}questions`).then((res) => res.data);
}

export async function getAQuestionById(id: number) {
  return await axios.get(`${baseUrl}questions/${id}`).then((res) => res.data);
}

// GET ENDPOINTS -- User
export async function getAllUsers() {
  return await axios.get(`${baseUrl}users`).then((res) => res.data);
}

export async function getAUserById(id: number) {
  return await axios.get(`${baseUrl}users/${id}`).then((res) => res.data);
}