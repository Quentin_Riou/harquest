import AutoForm, { AutoFormSubmit } from './ui/auto-form'
import * as z from 'zod'
import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogTrigger
} from './ui/alert-dialog'
import { Separator } from './ui/separator'
import { Button } from './ui/button'

const questionnaire = {
  question: 'Qui est le ',
  reponses: ['reponse1', 'reponse2', 'reponse3'],
  bonneReponse: 'reponse1',
  explication:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non roident, sunt in culpa qui officia deserunt mollit anim id est laborum',
  contexte: 'contexte'
} as const

const score = [] as number[]

// Define your form schema using zod
const formSchema = z.object({}).setKey(questionnaire.question, z.enum(questionnaire.reponses))

export const Autoform = () => {
  return (
    <AutoForm
      // Pass the schema to the form
      formSchema={formSchema}
      // You can add additional config for each field to customize the UI
      fieldConfig={{
        [questionnaire.question]: {
          fieldType: 'radio'
        }
      }}
      onSubmit={(values) => {
        // Do something with the values
        console.log(values)
        if (values[questionnaire.question] === questionnaire.bonneReponse) {
          score.push(1)
        }
      }}>
      <span className="flex	gap-4">
        <AlertDialog>
          <AlertDialogTrigger asChild>
            <Button>Voir l'explication</Button>
          </AlertDialogTrigger>
          <AlertDialogContent>
            <AlertDialogHeader>
              <AlertDialogTitle className="font-bold text-blue-500">
                {questionnaire.question}
              </AlertDialogTitle>
              <AlertDialogDescription>
                <span className="font-bold">
                  La bonne réponse est :{' '}
                  <span className="text-blue-500">{questionnaire.bonneReponse}</span>
                </span>
                <Separator className="mb-2" />
                <span className="grid text-justify">{questionnaire.explication}</span>
              </AlertDialogDescription>
            </AlertDialogHeader>
            <AlertDialogFooter>
              <AlertDialogAction>J'ai compris</AlertDialogAction>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialog>
        {/* 
      Pass in a AutoFormSubmit or a button with type="submit".
      Alternatively, you can not pass a submit button
      to create auto-saving forms etc.
      */}
        <AutoFormSubmit>Valider</AutoFormSubmit>
      </span>
    </AutoForm>
  )
}
