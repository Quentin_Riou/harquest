import { Autoform } from './components/AutoForm'
import './App.css'
import { BrowserRouter as Router } from 'react-router-dom'
import { Progress } from './components/ui/progress'

const value = 50

function App() {
  return (
    <div className="flex justify-center">
      <Router>
        <div className="flex w-2/3 flex-col gap-4">
          <h1 className="text-3xl font-bold">Questionnaire Harcèlement sexuel au travail</h1>
          <div>
            <p>{value} %</p>
            <Progress value={value} />
          </div>
          <Autoform />
        </div>
      </Router>
    </div>
  )
}

export default App
