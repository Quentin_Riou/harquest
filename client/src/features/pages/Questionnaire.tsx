import { RouterType } from '../types/RouterType'
import { Autoform } from '../../components/AutoForm'
import { useQuery } from '@tanstack/react-query'
import { getAllQuestions, getAQuestionById } from '../../api/getRequests'
import { QuestionProps, ReponseProps } from '../../api/types'

const id = 1

export const Questionnaire: RouterType[] = [
  {
    path: `question/${id}`,
    element: <Autoform />,
    title: 'questionnaire'
  }
]

export const Questions = () => {
  const queryGetAllQuestions = useQuery({
    queryKey: ['questions'],
    keepPreviousData: true,
    queryFn: getAllQuestions,
    retry : 3,
    cacheTime: 60 * 1000
  });

  const queryGetAQuestionById = useQuery({
    queryKey: ['question', { id }],
    keepPreviousData: true,
    queryFn: () => getAQuestionById(id),
    retry: 3,
    cacheTime: 60 * 1000
  });

  if (queryGetAllQuestions.status === 'loading') {
    return (
      <div className="d-flex align-items-center justify-content-center mt-4">
          <h3>Chargement...</h3>
      </div>
    );
  }

  if (queryGetAllQuestions.status === 'error') {
    return (
      <div className="d-flex align-items-center justify-content-center mt-4">
          <h3>Un problème technique est survenu.</h3>
          <h5>Désolé, nous n'avons pas pu charger les questions...</h5>
      </div>
    );
  }

  return (
    <div className="mt-4">
      <div className="relative px-2">
        {/* {user !== undefined ? ( */}
          <div>
            {queryGetAllQuestions.data?.map((questions: QuestionProps) => (
            ))}
          </div>
          <div>
            {queryGetAQuestionById.data?.reponses.map((reponse: ReponseProps) => (
            ))}
          </div>
        {/* )} */}
      </div>
    </div>
  );
}