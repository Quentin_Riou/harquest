import { Route, Routes } from 'react-router-dom'
import { RouterType } from '../types/RouterType'
import { Questionnaire } from '../pages/Questionnaire'

export const QuestionnaireRoute = () => {
  const pageRoutes = Questionnaire.map(({ path, title, element }: RouterType) => {
    return <Route key={title} path={`/${path}`} element={element} />
  })

  return <Routes>{pageRoutes}</Routes>
}
