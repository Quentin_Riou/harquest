package entities;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Reponse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ElementCollection
    private List<String> reponsePossible = new ArrayList<>();

    @Column(length = 500)
    private String explication = null;

    @ManyToOne
    @JoinColumn(name="ID_QUESTION")
    private Question question;

    public Reponse(String explication, Question question) {
        this.explication = explication;
        this.question = question;
    }

    public Reponse() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<String> getReponsePossible() {
        return reponsePossible;
    }

    public void setReponsePossible(List<String> reponsePossible) {
        this.reponsePossible = reponsePossible;
    }

    public String getExplication() {
        return explication;
    }

    public void setExplication(String explication) {
        this.explication = explication;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
}
