import entities.*;

import javax.persistence.*;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("pu-harquest");
        EntityManager em = emf.createEntityManager();

        //Insertions dans la base de données :
        EntityTransaction et = em.getTransaction();
        et.begin();
        //User user1 = new User("User1");
        //em.persist(user1);

        Question q1 = new Question("Un homme lui envoie des messages érotiques par SMS régulièrement et elle n’a jamais exprimé le souhait de les recevoir.", "De quoi s'agit-il ?", "Harcèlement sexuel");
        em.persist(q1);
        Reponse r1q1 = new Reponse("Le harcèlement sexuel représente des propos ou comportements à connotation sexuelle ou sexiste répétés qui soit portent atteinte à sa dignité en raison de leur caractère dégradant ou humiliant, soit créent à son encontre une situation intimidante, hostile ou offensante.", q1);
        r1q1.getReponsePossible().add("Agression sexuelle");
        r1q1.getReponsePossible().add("Harcèlement sexuel");
        r1q1.getReponsePossible().add("Drague");
        em.persist(r1q1);

        Question q2 = new Question("Elle lui a mis la main aux fesses discrètement dans son bureau.", "De quoi s'agit-il ?", "Agression sexuelle");
        em.persist(q2);
        Reponse r1q2 = new Reponse("L’agression sexuelle est un atteinte sexuelle sans pénétration commise sur une personne sans son consentement. Il s'agit d'un délit et la peine encourue est de 5 ans d’emprisonnement et 75 000€ d’amende majorée en cas de circonstances aggravantes.", q2);
        r1q2.getReponsePossible().add("Agression sexuelle");
        r1q2.getReponsePossible().add("Harcèlement sexuel");
        r1q2.getReponsePossible().add("Drague");
        em.persist(r1q2);

        Question q3 = new Question("Une collègue propose à un autre collègue d’aller boire un verre après le travail pour faire plus ample connaissance.", "De quoi s'agit-il ?", "Drague");
        em.persist(q3);
        Reponse r1q3 = new Reponse("Il s’agit d’une proposition, ce collègue peut accepter ou refuser.", q3);
        r1q3.getReponsePossible().add("Agression sexuelle");
        r1q3.getReponsePossible().add("Harcèlement sexuel");
        r1q3.getReponsePossible().add("Drague");
        em.persist(r1q3);

        Question q4 = new Question("Son patron lui a proposé une promotion en échange d’une soirée en tête à tête chez lui.", "De quoi s'agit-il ?", "Harcèlement sexuel");
        em.persist(q4);
        Reponse r1q4 = new Reponse("Est assimilé au harcèlement sexuel le fait, même non répété, d'user de toute forme de pression grave dans le but réel ou apparent d'obtenir un acte de nature sexuelle, que celui-ci soit recherché au profit de l'auteur des faits ou au profit d'un tiers.", q4);
        r1q4.getReponsePossible().add("Agression sexuelle");
        r1q4.getReponsePossible().add("Harcèlement sexuel");
        r1q4.getReponsePossible().add("Drague");
        em.persist(r1q4);

        Question q5 = new Question("Un collègue vous plaque contre un mur.", "De quoi s'agit-il ?", "Agression sexuelle");
        em.persist(q5);
        Reponse r1q5 = new Reponse("L’agression sexuelle est une atteinte sexuelle, c’est-à-dire un contact physique de nature sexuelle entre la victime et l'auteur des faits. En cas d’agression sexuelle, il faut alerter la police ou la gendarmerie et il est possible de déposer plainte.", q5);
        r1q5.getReponsePossible().add("Agression sexuelle");
        r1q5.getReponsePossible().add("Harcèlement sexuel");
        r1q5.getReponsePossible().add("Drague");
        em.persist(r1q5);

        Question q6 = new Question("Des collègues font régulièrement des remarques à un autre membre de l’équipe sur ses « gros seins ».", "De quoi s'agit-il ?", "Harcèlement sexuel");
        em.persist(q6);
        Reponse r1q6 = new Reponse("Est assimilé au harcèlement sexuel car il s’agit de propos ou comportements qui sont imposés à une même victime par plusieurs personnes, de manière concertée ou à l'instigation de l'une d'elles, alors même que chacune de ces personnes n'a pas agi de façon répétée.", q6);
        r1q6.getReponsePossible().add("Agression sexuelle");
        r1q6.getReponsePossible().add("Harcèlement sexuel");
        r1q6.getReponsePossible().add("Drague");
        em.persist(r1q6);

        Question q7 = new Question("Une collègue a utilisé un surnom affectueux pour s’adresser à son collègue.", "De quoi s'agit-il ?", "Drague");
        em.persist(q7);
        Reponse r1q7 = new Reponse(null, q7);
        r1q7.getReponsePossible().add("Agression sexuelle");
        r1q7.getReponsePossible().add("Harcèlement sexuel");
        r1q7.getReponsePossible().add("Drague");
        em.persist(r1q7);

        Question q8 = new Question("Un collègue a envoyé des photos de son corps à une collègue.", "De quoi s'agit-il ?", "Harcèlement sexuel");
        em.persist(q8);
        Reponse r1q8 = new Reponse("En cas d’harcèlement sexuel, il est possible d’alerter la police ou la gendarmerie et de déposer plainte. Il s’agit d’un délit et la peine encourue est de 2 ans d’emprisonnement et 30 000€ d’amende majorée en cas de circonstances aggravantes.", q8);
        r1q8.getReponsePossible().add("Agression sexuelle");
        r1q8.getReponsePossible().add("Harcèlement sexuel");
        r1q8.getReponsePossible().add("Drague");
        em.persist(r1q8);

        Question q9 = new Question("Un collègue a offert un bouquet de fleurs à une collègue.", "De quoi s'agit-il ?", "Drague");
        em.persist(q9);
        Reponse r1q9 = new Reponse("Il s’agit d’un geste affectueux qui peut précéder une relation.", q9);
        r1q9.getReponsePossible().add("Agression sexuelle");
        r1q9.getReponsePossible().add("Harcèlement sexuel");
        r1q9.getReponsePossible().add("Drague");
        em.persist(r1q9);

        Question q10 = new Question("Une collègue s’est assise sur les genoux d’un collègue en fauteuil roulant sans son consentement.", "De quoi s'agit-il ?", "Agression sexuelle");
        em.persist(q10);
        Reponse r1q10 = new Reponse("Une agression commise sur une personne particulièrement vulnérable, et dont la situation est apparente ou connue de l'auteur (âge, maladie, infirmité, déficience physique ou psychique, état de grossesse et précarité sociale ou économique) est une circonstance aggravante en cas d’agression sexuelle.", q10);
        r1q10.getReponsePossible().add("Agression sexuelle");
        r1q10.getReponsePossible().add("Harcèlement sexuel");
        r1q10.getReponsePossible().add("Drague");
        em.persist(r1q10);
        et.commit();

        //Réaliser une requête qui permet d’extraire tous les animaux d’une animalerie donnée
        /*TypedQuery<Animal> query1 = em.createQuery("select a from Animal a where a.petstore=" + petstore1.getId(), Animal.class);
        List<Animal> listAnimal = query1.getResultList();
        if (!listAnimal.isEmpty()){
            for (Animal a: listAnimal) {
                System.out.println("Id : " + a.getId());
            }
        }
        em.close();
        emf.close();*/

    }
}

